pdm check
Ignore docstring in class Meta and many others
Pylint rule for newline spacing / use match
https://github.com/pdm-project/pdm/pull/2394
https://github.com/pdm-project/pdm/pull/2408
Check default arguments to PDM Scripts
Logging
https://www.youtube.com/watch?v=9L77QExPmI0
"debug-in-terminal",

Use @override
Use pdm add flag to pin the version
Renovate group major peer dependencies
Generate XML report for unittests
Documentation setup:
	https://github.com/timvink/mkdocs-git-revision-date-localized-plugin
	Docstrings:
		accept-no-param-doc = false
		accept-no-raise-doc = false
		accept-no-return-doc = false
		accept-no-yields-doc = false
		default-docstring-type = 'sphinx', 'epytext', 'google', 'numpy', 'default'
	VSCode autoDocstring extension
Documentation write:
	MkDocs:
		Generalize blocks
		Plugins vs extensions
		MkDocs vs. Material for MkDocs
Pylint code quality on GitLab
Pylint --allow-reexport-from-package
Check argparse return values
Format imports
Auto formatting
git pre-commit hooks
VSCode devcontainer
Better development workflow for GitLab; Local runner?